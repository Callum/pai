#!/usr/bin/python3
from collections import defaultdict
import pai.base as pb


def make_freq_dict(sequence, kmerlen):
    kmerdict = defaultdict(int)
    for i in range(len(sequence) - kmerlen):
        kmerdict[sequence[i:i + kmerlen]] += 1

    total = sum(kmerdict.values())
    for key, value in kmerdict.items():
        kmerdict[key] = (kmerdict[key]/total)
    return kmerdict


def finddeviation(seq, windowsize, stepsize, kmerlen):

    def overall_dev(windowfreq, genomefreq):
        for window in windowfreq:
            for kmer in window[1].keys():
                window[1][kmer] = (window[1][kmer] / genomefreq[kmer])
        out = []
        for i in range(len(windowfreq)):
            out.append([])
            out[-1].append(windowfreq[i][0])
            devi = 0
            for v in windowfreq[i][1].values():
                if v != 0:
                    devi += abs(1-v)
            out[-1].append(devi)
        return out

    circular_seq = pb.circularise(seq, windowsize)
    window_frequencies = pb.find_freq(circular_seq, windowsize, stepsize,
                                      make_freq_dict, kmerlen)
    genome_freqency = pb.find_freq(seq, len(seq)-1, len(seq)-2, make_freq_dict, kmerlen)[0][1]
    return overall_dev(window_frequencies, genome_freqency)
