#!/usr/bin/python3
from math import log10


def log_model():
    """
    Hidden markov model parameters. Converts the probabilities to
    log10.
    :return: list containing states, start probabilities, transition
    probabilities, and observation likelihoods.
    """
    states = ('H', 'P')
    starts = {"H": 0.9749230605369226, "P": 0.025076939463077465}
    transitions = {
        "H": {"H": 0.9983980532839908, "P": 0.001601946716009232},
        "P": {"H": 0.06227925068180802, "P": 0.9377207493181919}}
    obslikelihood = {
        "H": {10101: 0.002536891881945978, 11000: 0.04951884153970707,
              11101: 0.00019204938544196658, 10100: 0.03347984286450283,
              10010: 0.00929772024729521, 11100: 0.002854290866269228,
              10110: 0.004322836166924266, 11011: 0.0014823452564951792,
              10111: 0.01597344888496357, 11111: 0.0009590969308898212,
              10011: 0.01902553911827482, 10001: 0.014889002355192464,
              11010: 0.0008705472142489145, 11001: 0.0010901965113711636,
              11110: 0.000528998307205417, 10000: 0.8429783524692721},
        "P": {10101: 0.007198104350158716, 11000: 0.1626950418026557,
              11101: 0.0018777663522153171, 10100: 0.023740331738722226,
              10010: 0.029507756963383557, 11100: 0.009344123038404793,
              10110: 0.007868735190235615, 11011: 0.043278043546295886,
              10111: 0.06809138462914115, 11111: 0.03970134573255242,
              10011: 0.06889614163723343, 10001: 0.04368042205034202,
              11010: 0.012876112129476461, 11001: 0.019135333303527517,
              11110: 0.005230920552599812, 10000: 0.4568784369830554}}

    for state in states:
        starts[state] = log10(starts[state])

        for key2 in transitions[state].keys():
            transitions[state][key2] = log10(transitions[state][key2])

        for key2 in obslikelihood[state].keys():
            obslikelihood[state][key2] = log10(obslikelihood[state][key2])
    return [states, starts, transitions, obslikelihood]


def log_viterbi(O, states, pi, A, B):
    """
    Viterbi decoding algorithm with log probabilities, according to
    "Speech and Language Processing." Daniel Jurafsky & James H. Martin, Chapter 8
    The states, transition probabilities, observation likelihoods and start state
    probabilities can be provided by model()
    :param O: Observation sequence
    :param state: A set of states
    :param A: Transition probability matrix (dict of dicts with state as a key
    :param B: Observation likelihoods (emissions)
    :param pi: start state probabilities
    :return: Q: State sequence
    """
    vpath = [{}]
    backpointer = [{}]
    Q = []
    # initialization
    for state in states:
        vpath[0][state] = pi[state] + B[state][O[0]]
        backpointer[0][state] = 0
    # recursion
    for t in range(1, len(O)):
        vpath.append({})
        backpointer.append({})
        for s_j in states:
            maxprob = max(vpath[t-1][s_i] + A[s_i][s_j] for s_i in states)
            vpath[t][s_j] = maxprob + B[s_j][O[t]]
            for s_i in states:
                if vpath[t-1][s_i] + A[s_i][s_j] == maxprob:
                    backpointer[t][s_j] = s_i
                    break
    # termination
    maxprob = max(list(vpath[-1].values()))
    for state in states:
        if vpath[-1][state] == maxprob:
            laststate = state
            Q.append(state)
            break

    for t in range(len(vpath) - 1, 0, -1):
        Q.append(backpointer[t][laststate])
        laststate = backpointer[t - 1][laststate]
    Q.reverse()
    return Q