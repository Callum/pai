#!/usr/bin/python3
from statistics import stdev, mean


def circularise(seq, windowsize):
    half_win = int(1 + (windowsize / 2))
    circular_sequence = seq[-half_win:] + seq + seq[:half_win]
    return circular_sequence


def compliment(sequence):
    return sequence[::-1].translate(str.maketrans('acgtACGT', 'tgcaTGCA'))


def find_freq(sequence, windowsize, stepsize, freqfun, *args):
    freqlist = []
    end = len(sequence) - windowsize

    for i in range(0, end, stepsize):
        freqlist.append([])
        freqlist[-1].append(i)
        freqlist[-1].append(freqfun(sequence[i:i+windowsize], *args))

    return freqlist


def find_deviant(pos_dev_arr):
    """Takes array of array [[pos1, pos2, deviation], etc] and returns
    array of array [[pos1, pos2, significantly deviant 1/0], etc]
    """
    variation = []
    for v in pos_dev_arr:
        variation.append(v[1])
    cutoff_value = mean(variation) + (1.65 * stdev(variation))
    for pos in pos_dev_arr:
        if pos[1] >= cutoff_value:
            pos[1] = 1
        else:
            pos[1] = 0
    return pos_dev_arr
