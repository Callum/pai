#!/usr/bin/python3
import subprocess
import io


def tRNApos_aragorn(sequence):
    def tempfile():
        tf = open('.temp', 'w')
        tf.write(">temp\n")
        tf.write(sequence)
        tf.close()
    tempfile()
    p = subprocess.Popen(['aragorn', '-w', '-t', '.temp'],
                         stdout=subprocess.PIPE)
    output = io.TextIOWrapper(p.stdout, encoding="utf-8")
    next(output)
    next(output)
    tRNApositions = []
    for line in output:
        position = line.split()[2]
        if position.startswith('c'):
            positions = position[2:-1].split(',')
            tRNApositions.append([int(positions[0]), int(positions[1]),
                                  '-'])
        else:
            positions = position[1:-1].split(',')
            tRNApositions.append([int(positions[0]), int(positions[1]),
                                  '+'])
    tRNApositions.sort(key=lambda x: x[0])
    return(tRNApositions)
