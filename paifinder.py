#!/usr/bin/python3
import pai.base as pb
import pai.GC as GC
import pai.tRNA as tRNA
import pai.codons as codons
import pai.kmer as kmer
import pai.blast as blast
import pai.HMM as hmm
import pai.plot as plot
import re
from collections import OrderedDict
from bisect import bisect_left


def get_seq(seqfile):
    sequence = ''
    jobname = ''
    f = seqfile.read()
    if type(f) is str:
        f = f.split('\n')
    else:
        f = f.decode('UTF-8', 'ignore').split('\n')
    for line in f:
        if line.startswith('>'):
            if jobname:
                break
            else:
                jobname = line.strip()[1:]
        else:
            sequence += line.rstrip().upper()
    seqlen = len(sequence)
    sequence = re.sub(r'[^AGCT]', '', sequence)
    if seqlen > len(sequence):
        return "Sequence contained non-standard nucleotides"
    else:
        return [sequence, jobname]


def get_pos(gfffile):

    digit_pat = re.compile(r'[^\d]')
    strand_pat = re.compile(r'[^(\+|\-)]')
    id_pat = re.compile(r'ID=(.+?);')

    positions = []
    f = gfffile.read()

    if type(f) is str:
        f = f.split('\n')
    else:
        f = f.decode('UTF-8', 'ignore').split("\n")

    for line in f:
        line = line.split("\t")
        if len(line) > 8:
            if line[2] == 'CDS':
                m = id_pat.match(line[8])
                if m:
                    idname = m.groups(0)[0]
                else:
                    idname = "None"
                start = int(digit_pat.sub('', line[3]))
                end = int(digit_pat.sub('', line[4]))
                strand = strand_pat.sub('', line[6])
                positions.append([start, end, strand, idname])
    return positions


def binary_obs_dict(blast, codon, kmer, gc):
    """Creates a dictionary with each position (stepsize) as the key
    and a binary sequence showing whether each feature is significant
    """
    def find_range_pos(pos1, pos2, poslist):
        def find_nearest_pos(pos):
            listpos = bisect_left(poslist, pos)
            if listpos == 0:
                return listpos
            elif listpos == len(poslist):
                return listpos
            elif (poslist[listpos] - pos) < (pos - poslist[listpos]):
                return listpos
            else:
                return listpos - 1
        start = find_nearest_pos(pos1)
        end = find_nearest_pos(pos2) + 1
        return poslist[start:end]

    blastpos_closest = []
    for pos in blast:
        if pos[2]:
            blastpos_closest += find_range_pos(pos[0], pos[1], [i[0] for i in gc])
    blastpos_closest = list(set(blastpos_closest))
    outdict = OrderedDict()

    for i in range(len(codon)):
        key = codon[i][0]
        if codon[i][1]:
            outdict[key] = 10100
        else:
            outdict[key] = 10000
        if kmer[i][1]:
            outdict[key] += 10
        if gc[i][1]:
            outdict[key] += 1

    for pos in blastpos_closest:
        outdict[pos] += 1000

    return outdict


def predictpai(sequence, genepos, windowsize, stepsize, kmerlen):
    seqlen = len(sequence)
    blastresults = blast.blast(sequence)
    tRNApos = tRNA.tRNApos_aragorn(sequence)
    genesdev = codons.get_codoncomp_genes(sequence, genepos, int(windowsize / 2))
    codondev = codons.window_deviation(genesdev, windowsize, stepsize, seqlen)
    codondev = pb.find_deviant(codondev)

    kmerdev = kmer.finddeviation(sequence, windowsize, stepsize, kmerlen)
    kmerdev = pb.find_deviant(kmerdev)

    gcfreq = GC.circle_gc_window(sequence, windowsize, stepsize)
    gcbinary = [list(i) for i in gcfreq]
    pb.find_deviant(gcbinary)

    binary_obs = binary_obs_dict(blastresults, codondev, kmerdev, gcbinary)
    outstates = hmm.log_viterbi(list(binary_obs.values()), *hmm.log_model())
    predictions = []
    for i in range(len(gcfreq)):
        if outstates[i] == "H":
            predictions.append([gcfreq[i][0], 0])
        else:
            predictions.append([gcfreq[i][0], 1])

    return [blastresults, tRNApos, codondev, kmerdev, gcfreq, predictions]


def make_chartdata(seqlen, jobname, blast, trna, codon, kmer, gc, pred):
    serieslist = plot.binary_series(blast, trna, codon, kmer, gc, pred)
    seriesstring = plot.series_string(serieslist)
    chartdata = plot.chart_string(jobname, seqlen, seriesstring)
    return chartdata


def merge_adjacent(pos_list):
    outpositions = []
    if pos_list[0][1]:
        outpositions.append(pos_list[0])
    for i in range(1, len(pos_list) - 1):
        if pos_list[i][1] != pos_list[i-1][1] or \
                        pos_list[i][1] != pos_list[i+1][1]:
            outpositions.append(pos_list[i])
    if pos_list[-1][1] and not pos_list[0][1]:
        outpositions.append(pos_list[-1])
    return outpositions


def find_pai_genes(genepos, pred_m, stepsize):
    """
    Finds the start and end of each predicted PAI, and appends a list of
    genes found within the PAI
    :param genepos: List of gene positions (as returned by get_pos())
    :param pred_m: A list of predicted PAIs, with positions merged by
                    merge_adjacent()
    :param stepsize: The stepsize, default 2000
    :return: Returns a list of lists of PAI start and ends and the genes found
             within.
    """
    halfstep = int(stepsize / 2)
    start = False
    end = False
    pred_st_en = []
    for i in range(1, len(pred_m)-1):
        if pred_m[i][1] and pred_m[i+1][1] and not pred_m[i-1][1]:
            start = pred_m[i][0] - halfstep
        elif pred_m[i][1] and pred_m[i-1][1] and not pred_m[i+1][1]:
            end = pred_m[i][0] + halfstep
        elif pred_m[i][1] and not pred_m[i-1][1] and not pred_m[i+1][1]:
            start = pred_m[i][0] - halfstep
            end = pred_m[i][0] + halfstep

        if start and end:
            pred_st_en.append([start, end])
            start = False
            end = False

    if start and pred_m[-1][1] and not end:
        pred_st_en.append([start, pred_m[-1][0] + halfstep])

    for pai in pred_st_en:
        pai.append([])
        for gene in genepos:
            if pai[0] <= gene[0] <= pai[1] or pai[0] <= gene[1] <= pai[1]:
                pai[2].append(gene[:4])
        if pai[2][0][0] < pai[0]:
            pai[0] = pai[2][0][0]
        if pai[2][-1][1] > pai[1]:
            pai[1] = pai[2][-1][1]
    return pred_st_en

def output_formats(pai_genes, outfmt):
    def summary():
        output_list = []
        output_list.append(["Feature", "ID", "Start", "End",
                            "Strand", "Likelihood"])
        for pai in pai_genes:
            output_list.append(["PAI", "", pai[0], pai[1], "", ""])
            for gene in pai[2]:
                output_list.append(["Gene", gene[3], gene[0], gene[1],
                                    gene[2], ""])
        return output_list


    if outfmt == 'sum':
        output = summary()

    return output


if __name__ == "__main__":
    import sys
    fastaname = sys.argv[1]
    gffname = sys.argv[2]
    windowsize = 10000
    stepsize = 2000
    kmerlen = 2

    fastafile = open(fastaname, 'r')
    with open(fastaname, 'r') as fastafile:
        sequence = get_seq(fastafile)[0]

    genepos = []
    gfffile = open(gffname, 'r')
    with open(gffname, 'r') as gfffile:
            genepos = get_pos(gfffile)
    gfffile.close()
    jobname = "Test"
    seqlen = len(sequence)
    posinfo = predictpai(sequence, genepos, 10000, 2000, 2)
    codons_merged = merge_adjacent(posinfo[2])
    kmer_merged = merge_adjacent(posinfo[3])
    pred_merged = merge_adjacent(posinfo[5])

    pai_genes = find_pai_genes(genepos, pred_merged, stepsize)
    print(pai_genes)
    pred_final = plot.parseprecise(pai_genes)
    chartdata = make_chartdata(seqlen, jobname, posinfo[0], posinfo[1],
                               codons_merged, kmer_merged, posinfo[4],
                               pred_final)
