#!/usr/bin/python3


def chart_string(title, seqlen, series):
    string = """chart: {{
            polar: true,
            type: 'bar',
            spacingBottom: 0,
            spacingTop: 10,
            spacingLeft: 10,
            spacingRight: 10,
            style: {{
                fontFamily: '"Raleway", sans-serif'
            }}
        }},
        exporting: {{
            sourceHeight: 800,
            sourceWidth: 800,
        }},
        title: {{
            text: '{}'
        }},
        xAxis: {{
            min: 0,
            max: {}
        }},
        yAxis: {{
            min: 0,
            max: 0.8
        }},
        series: {}""".format(title, seqlen, series)

    return string


def parseprecise(data):
    outdata = []
    for entry in data:
        outdata.append([entry[0] - 1, 0])
        outdata.append([entry[0], 1])
        outdata.append([entry[1], 1])
        outdata.append([entry[1] + 1, 0])
    return outdata


def binary_series(blast, trna, codon, kmer, gc, predictions):
    def makedata(data, maxy):
        for entry in data:
            if entry[1]:
                entry[1] = maxy
            else:
                entry[1] = 'null'
        return data

    blast = parseprecise(blast)
    trna = parseprecise(trna)

    def makeseries(name, data, miny, maxy, opacity):
        data = makedata(data, maxy)
        seriesdict = {
            'name': name,
            'type': 'area',
            'data': data,
            'threshold': miny,
            'fillOpacity': opacity,
        }
        return seriesdict

    gcseries = {
        'name': 'GC content',
        'type': 'line',
        'lineWidth': 1,
        'data': gc
    }

    blastseries = makeseries('Blast hits', blast, 0.75, 0.8, 0.8)
    trnaseries = makeseries('tRNA genes', trna, 0.25, 0.3, 1.0)
    codonseries = makeseries('Codon deviation', codon, 0.7, 0.75, 0.8)
    kmerseries = makeseries('Kmer deviation', kmer, 0.65, 0.7, 0.8)
    predictionseries = makeseries('Predicted PAIs', predictions, 0.8,
                                  0.85, 0.8)

    return [blastseries, trnaseries, codonseries, kmerseries, gcseries,
            predictionseries]


def series_string(chartdict):
    string = '['
    for series in chartdict:
        string += '{'
        for key in series.keys():
            string += '{}: '.format(key)
            if type(series[key]) is str:
                string += '"{}"'.format(series[key])
            elif type(series[key]) is list:
                string += '['
                for pos in series[key]:
                    string += '[{}, {}],'.format(pos[0], pos[1])
                string += ']'
            else:
                string += str(series[key])
            string += ','
        string += 'lineWidth: 1,'
        if series['name'] != 'tRNA genes':
            string += 'marker: {enabled: false, radius: 1.5}},'
        else:
            string += 'marker: {radius: 1.5}},'
    string += ']'
    return string
