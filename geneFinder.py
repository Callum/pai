#!/usr/bin/python3
import re
from math import log
from pai.base import compliment


def mainloop(sequence, minorf, sdp, codons):
    def findstart(startcodons, sequence):
        startlocations = []
        for match in startcodons.finditer(sequence):
            startlocations.append(match.start())
        return startlocations


    def sdpwm(upstreamseq, sdp):
        seqlen = len(upstreamseq)
    
        def match(y):
            prob = 0
            for i in range(sdlen):
                prob += (logsd[i][upstreamseq[y+i]])
            return (prob-lop)/(hip-lop)
    
        for x in range(seqlen-sdlen+1):
            p = match(x)
            if p > sdp:
                return True
        return False
    
    
    def sdcheck(startlocations, sequence, sdp):
        foundsd = []
        for _ in range(len(startlocations)):
            position = startlocations.pop(0)
            upstreamseq = sequence[position-20:position-5]
            p = sdpwm(upstreamseq, sdp)
            if p:
                foundsd.append(position)
        return foundsd
    
    
    def getuniq(genelocations):
        uniq = {}
        dellist = []
        for k in genelocations.keys():
            end = genelocations[k][0]
            if end not in uniq.keys():
                uniq[end] = k
            else:
                if genelocations[k][1]:
                    if k < uniq[end]:
                        dellist.append(uniq[end])
                        uniq[end] = k
                    else:
                        dellist.append(k)
                else:
                    if k > uniq[end]:
                        dellist.append(uniq[end])
                        uniq[end] = k
                    else:
                        dellist.append(k)
        for k in dellist:
            del genelocations[k]
        return genelocations
    
    
    def findend(sdstartlocs, sequence, minorf, strand):
        genelocations = {}
        seqlen = len(sequence)
        for startloc in sdstartlocs:
            stoplocs = []
            for match in stopcodons.finditer(sequence[startloc:startloc+5000]):
                stoplocs.append(match.start())
            for stoploc in stoplocs:
                if stoploc % 3 == 0:
                    if stoploc < minorf:
                        break
                    if strand:
                        genelocations[startloc] = [startloc+stoploc, strand]
                    else:
                        genelocations[seqlen-startloc-stoploc-1] = \
                            [seqlen-startloc-1, strand]
                    break
        genelocations = getuniq(genelocations)
        return genelocations
    
    
    def makeoutput(genelocs):
        genearray = []
        skeys = sorted(genelocs.keys())
        for i in range(len(skeys)):
            genearray.append([])
            genearray[i].append(skeys[i])
            genearray[i].append(genelocs[skeys[i]][0])
            if genelocs[skeys[i]][1]:
                genearray[i].append('+')
            else:
                genearray[i].append('-')
            genearray.append('Gene {}'.format(i))
        return genearray
    
    
    def output(outdic):
        keys = sorted(outdic.keys())
        for i in range(len(keys)):
            print("X\tPGF\tCDS\t{!s}\t{!s}\t-\t{!s}\t0\tID=GENE{!s}"
                  .format(keys[i], outdic[keys[i]][0], outdic[keys[i]][1], i + 1))
    

    sdmatrix = [
        {'A': 27, 'T': 38, 'G': 20, 'C': 15},
        {'A': 38, 'T': 0, 'G': 32, 'C': 30},
        {'A': 76, 'T': 10, 'G': 7, 'C': 7},
        {'A': 0, 'T': 0, 'G': 99, 'C': 0},
        {'A': 0, 'T': 0, 'G': 98, 'C': 2},
        {'A': 85, 'T': 11, 'G': 2, 'C': 2},
        {'A': 19, 'T': 15, 'G': 60, 'C': 6}]

    stopcodons = re.compile('TAG|TAA|TGA')

    strand = True
    lop = 0
    hip = 0
    sdlen = len(sdmatrix)

    logsd = []
    for i in range(sdlen):
        logsd.append({})
        for j in (sdmatrix[i].keys()):
            logsd[i][j] = log(float(sdmatrix[i][j] /
                                    sum(sdmatrix[i].values())+0.0000001))

    for i in range(sdlen):
        lop += min(logsd[i].values())
        hip += max(logsd[i].values())

    startcodons = {
            1: 'ATG',
            2: 'ATG|GTG',
            3: 'ATG|GTG|TTG'
        }[codons]
    startcodons = re.compile(startcodons)
    compseq = compliment(sequence)
    genelocs = {}
    genelocs = findend(
        sdcheck(
            findstart(startcodons, sequence),
            sequence, sdp),
        sequence, minorf, strand)
    strand = False
    genelocs.update(findend(
        sdcheck(findstart(startcodons, compseq),
            compseq, sdp), compseq, minorf, strand))
    return(makeoutput(genelocs))
