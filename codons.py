#!/usr/bin/python3
from collections import defaultdict
import pai.base as pb

codontable = {
    'F': ['TTT', 'TTC'],
    'L': ['TTA', 'TTG', 'CTT', 'CTC', 'CTA', 'CTG'],
    'I': ['ATT', 'ATC', 'ATA'],
    'M': ['ATG'],
    'V': ['GTT', 'GTC', 'GTA', 'GTG'],
    'S': ['TCT', 'TCC', 'TCA', 'TCG', 'AGT', 'AGC'],
    'P': ['CCT', 'CCC', 'CCA', 'CCG'],
    'T': ['ACT', 'ACC', 'ACA', 'ACG'],
    'A': ['GCT', 'GCC', 'GCA', 'GCG'],
    'Y': ['TAT', 'TAC'],
    'OC': ['TAA'],
    'AM': ['TAG'],
    'OP': ['TGA'],
    'H': ['CAT', 'CAC'],
    'Q': ['CAA', 'CAG'],
    'N': ['AAT', 'AAC'],
    'K': ['AAA', 'AAG'],
    'D': ['GAT', 'GAC'],
    'E': ['GAA', 'GAG'],
    'C': ['TGT', 'TGC'],
    'W': ['TGG'],
    'R': ['CGT', 'CGC', 'CGA', 'CGG', 'AGA', 'AGG'],
    'G': ['GGT', 'GGC', 'GGA', 'GGG']}

revcodontable = {
    'TTT': 'F', 'TTC': 'F', 'TTA': 'L', 'TTG': 'L',
    'CTT': 'L', 'CTC': 'L', 'CTA': 'L', 'CTG': 'L',
    'ATT': 'I', 'ATC': 'I', 'ATA': 'I', 'ATG': 'M',
    'GTT': 'V', 'GTC': 'V', 'GTA': 'V', 'GTG': 'V',
    'TCT': 'S', 'TCC': 'S', 'TCA': 'S', 'TCG': 'S',
    'CCT': 'P', 'CCC': 'P', 'CCA': 'P', 'CCG': 'P',
    'ACT': 'T', 'ACC': 'T', 'ACA': 'T', 'ACG': 'T',
    'GCT': 'A', 'GCC': 'A', 'GCA': 'A', 'GCG': 'A',
    'TAT': 'Y', 'TAC': 'Y', 'TAA': 'OC', 'TAG': 'AM',
    'CAT': 'H', 'CAC': 'H', 'CAA': 'Q', 'CAG': 'Q',
    'AAT': 'N', 'AAC': 'N', 'AAA': 'K', 'AAG': 'K',
    'GAT': 'D', 'GAC': 'D', 'GAA': 'E', 'GAG': 'E',
    'TGT': 'C', 'TGC': 'C', 'TGA': 'OP', 'TGG': 'W',
    'CGT': 'R', 'CGC': 'R', 'CGA': 'R', 'CGG': 'R',
    'AGT': 'S', 'AGC': 'S', 'AGA': 'R', 'AGG': 'R',
    'GGT': 'G', 'GGC': 'G', 'GGA': 'G', 'GGG': 'G'}


def get_codoncomp_genes(sequence, positions, halfwin):

    def codoncomp(seq):
        codondic = defaultdict(int)
        for key in revcodontable.keys():
            codondic[key] = 0
        for i in range(0, len(seq) - 2, 3):
            codondic[seq[i:i + 3]] += 1
        return codondic

    for gene in positions:
        if gene[2] == '+':
            gene.append(sequence[gene[0]:gene[1]])
        else:
            gene.append(pb.compliment(sequence[gene[0]:gene[1]]))
    for gene in positions:
        gene.append(codoncomp(gene[4]))

    positions.sort(key=lambda x: x[0])
    seqlen = len(sequence)

    startgenes = []
    endgenes = []

    for gene in positions:
        if gene[0] <= halfwin:
            newgene = gene
            newgene[0] += seqlen
            newgene[1] += seqlen
            endgenes.append(newgene)
        elif gene[0] >= (seqlen - halfwin):
            newgene = gene
            newgene[0] -= seqlen
            newgene[1] -= seqlen
            startgenes.append(newgene)

    return startgenes + positions + endgenes


def window_deviation(genes_comp, windowsize, stepsize, seqlen):
    def get_codoncomp_window(genes_comp, windowsize, stepsize, seqlen):
        window_comp = []
        for i in range(0, seqlen, stepsize):
            j = int(i / stepsize)
            window_comp.append([i, int(i + windowsize),
                                defaultdict(int)])
            for g in genes_comp:
                if (i - (windowsize / 2)) < g[0] < (i + (windowsize / 2)):
                    for key in g[5]:
                        window_comp[j][2][key] = window_comp[j][2][key] + g[5][key]
        return window_comp

    def codonperaafreq(dic):
        freqdic = defaultdict(float)
        for codon in dic.keys():
            total = 0.0000000000000001
            for x in codontable[revcodontable[codon]]:
                total += dic[x]
            freqdic[codon] = (dic[codon] / total)
        return freqdic

    def relativefreq(window_comp, genome_freq):
        for window in window_comp:
            for codon in window[2].keys():
                window.append(codonperaafreq(window[2]))
                window[3][codon] = window[3][codon] / genome_freq[codon]
        return window_comp

    def overall_dev(window_relfreq):
        outwin = []
        for i in range(len(window_relfreq)):
            outwin.append([window_relfreq[i][0]])
            devi = 0
            try:
                for v in window_relfreq[i][3].values():
                    if v != 0.0:
                        devi += abs(1-v)
            except IndexError:
                pass
            outwin[i].append(devi)
        return outwin

    window_comp = get_codoncomp_window(genes_comp, windowsize, stepsize, seqlen)
    # lastpos = window_comp[-1][1]
    genome_comp = get_codoncomp_window(genes_comp, seqlen, seqlen-1, seqlen)[0][2]
    genome_freq = codonperaafreq(genome_comp)
    window_relfreq = relativefreq(window_comp, genome_freq)
    return overall_dev(window_relfreq)
