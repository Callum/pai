#!/usr/bin/python3
from subprocess import Popen, PIPE


def blast(sequence):
    blastresults = []
    blaststarts = []
    bseq = bytes(sequence, 'utf-8')
    p = Popen(['blastn', '-query', '-', '-db', 'NucVirDb',
               '-outfmt', '7', '-evalue', '0.001'], stdout=PIPE, stdin=PIPE)
    blastout = p.communicate(input=bseq)[0]
    blastout = blastout.decode()
    blastout = blastout.split('\n')
    for results in blastout:
        results = results.split('\t')
        if results[0] == 'Query_1':
            res = [int(results[6]), int(results[7]), 1]
            if res[0] not in blaststarts:
                blaststarts.append(res[0])
                blastresults.append(res)
    blastresults.sort(key=lambda x: x[0])
    return blastresults
