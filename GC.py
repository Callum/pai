#!/usr/bin/python3
import pai.base as pb


def gc_content(sequence):
    return (sequence.count('G') + sequence.count('C')) / len(sequence)


def circle_gc_window(sequence, windowsize, stepsize):
    circular_sequence = pb.circularise(sequence, windowsize)
    gc_freq_list = pb.find_freq(circular_sequence, windowsize, stepsize, gc_content)
    return gc_freq_list
